package main

import (
	"bufio"
	"net"
	"strconv"
)
import "fmt"
import "strings" // only needed below for sample processing


func SplitString(s string) ([] int64){
	//x := strconv.ParseInt(strings.Split(s,","),10,64);
	var intArray [] int64
	var  x [] string = strings.Split(s,",")
	for i := 0; i < len(x); i++ {
		var y,_ = strconv.ParseInt(x[i],10,64);
		intArray = append(intArray, y)
	}

	return intArray;
}

func PrimeElement(num int64) (bool){
	var i int64
	for i =1; i <= num/2; i++ {
		if num % i == 0{
			break;
		}

	return false;
	}
	return true;
}

func PrimeElements(intArray []int64) (int){

	var numberOfPrimeNumber int = 0
	for i:=1; i <=len(intArray); i++{
		if(PrimeElement(intArray[i])){
			numberOfPrimeNumber++
		}
	}
	return numberOfPrimeNumber
}


func main() {

	fmt.Println("Launching server...")

	// listen on all interfaces
	ln, _ := net.Listen("tcp", ":8081")

	// accept connection on port
	conn, _ := ln.Accept()

	// run loop forever (or until ctrl-c)
	for {
		// will listen for message to process ending in newline (\n)
		message, _ := bufio.NewReader(conn).ReadString('\n')
		// output message received
		fmt.Print("Message Received:", string(message))
		// sample process for string received

		var asd [] int64 = SplitString(message)
		var numb int = PrimeElements(asd)

		// send new string back to client
		conn.Write([]byte(strconv.Itoa(numb)))
	}



}